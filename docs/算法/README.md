# 数据结构与算法

## 数据结构


1. 线性表
   1. 顺序存储
      1. 顺序表
   2. 链式存储
      1. 指针实现
         1. 单链表
         2. 双链表
         3. 循环链表
      2. 数组实现
         1. 静态链表   
2. 树
   1. 二叉树
      1. 线索二叉树
      2. 排序二叉树
         1. 二叉排序树
         2. AVL(平衡二叉)树: 一个二叉树每个节点的左右两个子树的高度差的绝对值不超过1。
      3. BTree
         1. B-Tree 
         2. B+Tree
         3. B*Tree
      4. 哈夫曼树
      5. 红黑树
   2. 多叉树
3. 图
   1. 图的存储
      1. 邻接矩阵，邻接表
      2. 邻接多重表，狮子链表
   2. 图的遍历
      1. BFS
      2. DFS
   3. 应用
      1. 最小生成树
         1. Prim算法
         2. Kruskal算法
      2. 最短路径
         1. Dijkstra算法
         2. Floy算法
      3. 拓扑排序 AOV网
      4. 关键路径 AOE网
4. 排序
    1. comparison sort
        1. 插入排序 insertion
            1. 插入排序 insertion sort
            2. 希尔排序 shell sort
        2. 交换排序 swap
            1. 冒泡排序 bubble sort
            2. 快速排序 quick sort
        3. 选择排序
            1. 选择排序 selection sort
            2. 堆排序 heap sort
        4. 归并排序 merge sort
    2. integer sort
        1. 计数排序 counting sort
        2. 桶排序 bucket sort
        3. 基数排序 radix sort
5. 堆/栈/队列
   1. 栈
      1. 顺序栈
      2. 链栈
      3. 共享栈
   2. 队列
      1. 链式队列
      2. 循环队列
      3. 双端队列
   3. 堆
       1. 大根堆
       2. 小根堆
6. 查找
   1. 顺序查找
   2. 折半查找
   3. 分块查找
   4. Hash查找


## 算法

力扣精选
1. [初级算法](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2zsx1/) 帮助入门
2. [算法面试题汇总](https://leetcode-cn.com/leetbook/detail/top-interview-questions/)
3. [LeetCode 热题 HOT 100](https://leetcode-cn.com/problem-list/2cktkvj/)
4. [腾讯各事业群爱考的算法题Top10](https://leetcode-cn.com/circle/discuss/q1iaUL/)


算法资料

1. 剑指offer [C++版本](https://github.com/hbulpf/CodingInterviewChinese2) / [Java版本](https://github.com/hbulpf/point-to-offer-edition2)
2. [leetcode解题之路](https://gitee.com/golong/leetcode)
3. [图解力扣算法](https://github.com/MisterBooo/LeetCodeAnimation)
4. [leetcode-python](https://github.com/shichao-an/leetcode-python)
5. [leetbook](https://github.com/hk029/leetbook)
6. [awesome-java-leetcode](https://github.com/Blankj/awesome-java-leetcode)
7. [Leetcode-Java](https://github.com/dingjikerbo/Leetcode-Java)
8. [leetcode-editor](https://github.com/shuzijun/leetcode-editor)
9. [LeetCode题目分类与面试问题整理](https://github.com/yuanguangxin/LeetCode)
10. [软件工程技术面试个人指南](https://github.com/kdn251/interviews)
11. [labuladong 的算法小抄](https://labuladong.gitee.io/algo/)
12. [全栈潇晨](https://xiaochen1024.com/)
13. [Python算法与数据结构视频教程](https://github.com/PegasusWang/python_data_structures_and_algorithms): [博客](https://pegasuswang.github.io/python_data_structures_and_algorithms/)
14. [PADS, Python algorithms and data structures](https://www.ics.uci.edu/~eppstein/PADS/). David Eppstein


## 参考

1. [常见排序算法复杂性分析](https://blog.csdn.net/yushiyi6453/article/details/76407640)
