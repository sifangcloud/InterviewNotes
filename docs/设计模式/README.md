# 设计模式

23中设计模式口诀:
1. 创建型: 两厂建单元(楼)
   1. 工厂方法(Factory Method)
   2. 抽象工厂(Abstract Factory)
   3. 建造者(Builder)
   4. 单例(Singleton)
   5. 原型(Prototype)