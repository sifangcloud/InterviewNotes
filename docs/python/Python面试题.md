# Python面试题

## 基础篇

1、为什么学习Python？

```
Python是一门优秀的综合语言， Python的宗旨是简明、优雅、强大，<br>在人工智能、云计算、金融分析、大数据开发、WEB开发、自动化运维、测试等方向应用广泛
```

2、通过什么途径学习的Python？

```
通过自学（博客、视频、github等）
```


## 参考

1. [python面试题——基础篇（80题）](https://www.cnblogs.com/xiugeng/p/9712775.html)