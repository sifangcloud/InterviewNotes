// .vuepress/config.js
module.exports = {
    title: 'InterviewNotes', 
    description : '面试笔记',
    base : '/interviewnotes/',
    markdown: {
      lineNumbers: true
    },
    themeConfig: {
      sidebarDepth: 4,
    //   logo: '/assets/img/logo.png',
      nav: [
        { text: '算法', link: '/算法/' },
        { text: '计算机网络', link: '/计算机网络/' },
        { text: '操作系统', link: '/操作系统/' },
        { text: '设计模式', link: '/设计模式/' },
        { text: '数据库', link: '/数据库/' },
        {
            text: '其它',
            ariaLabel: 'others',
            items: [
              { text: 'C++', link: '/cpp/' },
              { text: 'Python', link: '/python/' },
              { text: 'Java', link: '/Java/' },
              { text: 'Android', link: '/Android/' },
              { text: 'hadoop', link: '/hadoop/' },
              { text: '面经', link: '/面经/' },
              { text: '运维开发', link: '/运维开发/' }
            ]
        }
      ],
      sidebar: {
        '/算法/': [
          '',  
          '数据结构',
          '算法',
          '常见算法题', 
          'Tree',
        ],
        '/计算机网络/': [
          '',    
          'TCP_IP协议各层详解', 
          '计算机网络面试题'
        ],        
        '/操作系统/': [
          '',    
          '操作系统面试题',
          'Linux命令',
       ],
       '/设计模式/': [
        ''
      ],
       '/数据库/': [
        '',    
        'MySQL面试题', 
        'Redis面试题'
        ],       
        '/Android/': [
          '',  
          'Android基础', 
          'Android面试题',
        ],
        '/cpp/': [
          '',  
          '经典面试题'
        ],
        '/python/': [
          '',  
          'Python面试题'
        ],
        '/Java/': [
          '',    
          '设计模式', 
          'Java基础面试',
          'Java面试题2',
          'Java面试题3',
          'JVM',
          'NIO',
          'Spring',
        ],        
        '/面经/': [
          '',    
          '岗位jd',
          '求职准备',
          '开发工具',
          'hr_questions'
        ],
        '/hadoop/': [
          '',  
          '大数据面试题', 
          'Hadoop面试题' 
        ],
        '/运维开发/': [
          '',  
          'Docker使用', 
          '负载均衡',
          '运维开发面试题'
        ],
        '/recommand/': [
          '',  
          '字节跳动推荐'
        ],
        '/': [
          '',    
          '/算法/',
          '/操作系统/',
          '/计算机网络/',
          '/设计模式/',
          '/数据库/',
          '/cpp/',
          '/python/',
          '/Java/',
          '/hadoop/',
          '/Android/',
          '/面经/',
          '/recommand/'
        ]
      },
      lastUpdated: 'Last Updated', // string | boolean
      nextLinks: true, // 上/下一篇链接
      prevLinks: true, // 页面滚动
      smoothScroll: true
    }
  }