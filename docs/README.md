# 面试笔记

[![知识共享协议（CC协议）](https://img.shields.io/badge/License-Creative%20Commons-DC3D24.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh)
[![GitHub stars](https://img.shields.io/github/stars/hbulpf/InterviewNotes.svg?label=Stars)](https://github.com/hbulpf/InterviewNotes)
[![GitHub watchers](https://img.shields.io/github/watchers/hbulpf/InterviewNotes.svg?label=Watchers)](https://github.com/hbulpf/InterviewNotes/watchers)
[![GitHub forks](https://img.shields.io/github/forks/hbulpf/InterviewNotes.svg?label=Forks)](https://github.com/hbulpf/InterviewNotes/fork)


校招基础知识总结与整理，助你成为 offer 收割机。

总体资料参考

1. [计算机科学学习笔记](https://github.com/hbulpf/CS-Notes)
2. [后台架构师成长](https://github.com/hbulpf/ArchitectRoute)

算法
1. [labuladong 的算法小抄](https://labuladong.gitee.io/algo/)

----------------------------------------

**项目规范**

本文使用 [`Markdown`](https://www.markdownguide.org/basic-syntax) 编写, 排版符合[`中文技术文档写作规范`](https://github.com/hbulpf/document-style-guide)。Find Me On [**Github**](https://github.com/hbulpf/InterviewNotes) , [**Gitee**](https://gitee.com/sifangcloud/InterviewNotes)

**友情贡献**

@[**RunAtWorld**](http://www.github.com/RunAtWorld)  &nbsp;  @[**wiggins**](https://github.com/GroundWu)  &nbsp;  @[**Clock966**](http://www.github.com/Clock966) 
