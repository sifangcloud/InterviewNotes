# 字节跳动推荐
> 字节跳动技术 Leader 们推荐的学习资源
“春季招聘和金三银四要开始了，我想提升技术，更上一层楼，除了投简历刷题，还有什么可以努力的方向啊？”

如果你是技术领域的新人，或者已经毕业多年、正在考虑转向新的技术方向，上面这个问题可能正在困扰着你。

为了回答这个问题，技术范儿找到了多媒体、推荐算法、计算机视觉、强化学习、机器翻译、知识图谱、安卓、iOS、前端等几个方向的Leader，推荐了各个技术方向的自学资源。

其中，有不少业界知名的书籍、全球CS名校的公开课程，可以系统性地帮你了解一个领域的全貌。

还有不少应用技术和开源项目，工业界的常备工具都列齐了。

另外，也有一些是Leader们推荐团队内部同学学习的资料，如果你理解掌握得足够好，说不定可以在面试官心目中留下不错的印象。

如果你是正在准备春招的在校生，或者已经毕业并希望在金三银四获得更好的平台，不妨收藏起来，慢慢学习。
## 多媒体

多媒体团队Leader Jessica推荐了三类内容：

第一类是图像、视频处理的基础理论书籍；

第二类是视频编码标准方面的书籍；

第三类是业内常用的开源项目。

**《数字图像处理》**

作者：Rafael C. Gonzalez 等

![图片](./imgs/640.png)

数字图像处理领域的经典教材，也普遍作为学习图像处理、计算机视觉的入门必备书籍，经久不衰。

**《视频处理与通信》**

作者：王瑶 等

![图片](./imgs/640-1616083658214.png)

系统性介绍视频处理关键技术的一本经典教材，从视频基础理论到视频编码、视频通信等。适合有一点信号系统或者图像处理理论基础的同学进一步学习使用。

**《新一代视频压缩编码标准--H.264/AVC》**

作者：毕厚杰、王健

![图片](./imgs/640-1616083658272.png)

一本能让读者很好地了解主流视频编码技术的参考书。

**《Audio Signal Processing and Coding》**

作者：Andreas Spanias, Ted Painter, Venkatraman Atti

![图片](./imgs/640-1616083658299.png)

音频的书相比视频而言少很多，但这本算是音频信号处理与编码领域较为经典的了，可以作为音频处理学习相关的参考书。

**相关开源工程**

FFmpeg：https://github.com/FFmpeg/FFmpeg

（复制链接地址到浏览器查看）

迄今为止最流行的开源多媒体框架之一，非常强大，基本算是互联网视频技术相关的必修开源工程了。

vlc：https://github.com/videolan/vlc

Ijkplayer：https://github.com/bilibili/ijkplayer

exoplayer：https://github.com/google/ExoPlayer

播放器相关三大经典开源工程，了解主流视频解码、播放技术必选。

Jessica说，上述这些开源工程基本上是各个方向上不同时期最top的了，业内从业者对它们都非常熟悉。

## 推荐算法

抖音推荐团队Leader William同学推荐了5本书，基本都是深度学习、机器学习方面非常经典的书。

**《Deep Learning深度学习》**

作者：Ian Goodfellow、Yoshua Bengio、Aaron Courville

![图片](./imgs/640-1616083658320.png)

这本书就是业内知名的「花书」，是深度学习领域奠基性的经典教材。

**《动手学深度学习》**

作者：李沐 等

![图片](./imgs/640-1616083658384.png)

William说，这是他见过最好的机器学习、深度学习教材，理论与实践结合，并且中英双语都有，而且还是免费开源的资源。

资源链接

电子版：https://zh.d2l.ai/index.html

GitHub：https://github.com/d2l-ai/d2l-zh

**《百面机器学习》**

作者：诸葛越

![图片](./imgs/640-1616083658385.png)

William说，这本书的作者是Hulu中国负责人，书里知识点很多，也有不少工业界的观点，相关知识点最好都弄清楚，对从事算法工作会有比较大的帮助。

**《深度学习推荐系统》**

作者：王喆

![图片](./imgs/deepRecommandSys.png)

William认为，这本书可以帮助读者了解业界推荐系统的基础知识体系，梳理推荐算法的发展脉络。

**《推荐系统实践》**

作者：项亮

![图片](./imgs/640-1616083658385.png)

推荐系统入门必备，非常适合初学者。

## 计算机视觉

图像算法方向的Leader吴辛隆说，团队同学主要靠学习CV论文来提升自己的技术能力。除了历年顶会的论文之外，他也推荐了业界最常用的PyTorch和TensorFlow两大框架，以及影响力最大的几位专家的书籍课程。

**吴恩达深度学习工程师全套课程**

主讲：吴恩达

桃李满天下的吴恩达老师的课程，深度学习方向的同学应该都听说过。

链接：https://mooc.study.163.com/smartSpec/detail/1001319001.htm

**斯坦福 CS231n：用于视觉识别的卷积神经网络**

主讲：李飞飞

斯坦福一大知名课程，主讲人是推动了CV行业飞速发展的ImageNet发起人李飞飞。

链接：http://cs231n.stanford.edu/

**《机器学习》**

作者：周志华

![图片](./imgs/640-1616083658386.png)

这本书也是业界知名的教材，它从“如何挑西瓜”这个例子开头，又被称作「西瓜书」，来自南京大学周志华教授。

**PyTorch教程-Yunjey Choi**

![图片](./imgs/PyTorch.png)

PyTorch作为备受欢迎的深度学习两大框架之一，对于计算机视觉等方向的研究者来说是必备技能。如果你已经看过了PyTorch官方教程，来自韩国NAVER AI Lab研究员Yunjey Choi的开源PyTorch教程是不错的补充，在GitHub上有将近20000颗星，教程中的大多数模型是由不到30行代码实现的。

链接：https://github.com/yunjey/pytorch-tutorial

**TensorFlow中文官方文档**

![图片](./imgs/640-1616083658387.png)

深度学习另一大框架TensorFlow官方教程的中文版，由极客学院Wiki翻译。

链接：https://github.com/jikexueyuanwiki/tensorflow-zh

**CVF顶会论文库**


计算机视觉基金会（CVF）赞助了包括CVPR、ICCV等在内业界主流的几大计算机视觉顶会，他们的论文库也结构性地整理了这些顶会历年的所有论文，可以方便同学们系统性的查找计算机视觉相关各方面的论文。

链接：https://openaccess.thecvf.com/menu

## 强化学习

强化学习研究员Flood Sung和ChnX两位的推荐名单里除了一本名为《强化学习》的经典书目之外，还有斯坦福和UC伯克利的开源教学内容。

**《Reinforcement Learning: An Introduction》**

作者：Richard S. Sutton and Andrew G. Barto

![图片](./imgs/ReinforcementLearning.png)

这本书是强化学习最全面、最基础的教材，两位研究员强烈建议每一位同学通读一至两遍英文原版。

前面抖音推荐团队Leader William也非常推荐这本书，这本强化学习综述书, 可以帮助初学者建立相关知识体系。

英文原版：http://incompleteideas.net/book/the-book.html

相关课程：https://www.davidsilver.uk/teaching/

**UC伯克利CS285：Deep Reinforcement Learning**

主讲：Sergey Levine

这套课程包含23节课程和5个课后作业，适合对强化学习、机器学习有一定了解的人。

链接：http://rail.eecs.berkeley.edu/deeprlcourse/

**斯坦福CS 330：Deep Multi-Task and Meta Learning**

主讲：Chelsea Finn

CS 330整体课程比较长，大约需要3个月的学习时间，不过两位研究员更推荐课程中Meta-RL的部分，压力会小一点。

链接：https://cs330.stanford.edu/

## 机器翻译

机器翻译团队Leader王萱选择了经典的书目和课程。

**《统计学习方法》**

作者：李航

![图片](./imgs/640-1616083658435.png)

王萱认为，这本书由浅入深，涉及的知识面非常广，算法包括NB、LR、SVM、CART、GBDT、感知机、最大熵、EM、HMM、CRF等，书中例子、推导、算法介绍相对比较详细，适合花比较完整的时间，细细赏读。

另外，这本书还有一个优势是网上的解析、课程甚至所有算法的GitHub开源都非常完整，碰到难懂的问题，都可以很快找到答案。

前面抖音推荐团队Leader William同学也认为，这本书每个模型讲的很透彻，对提升机器学习背后的数学能力有帮助。

**斯坦福CS 224N：Natural Language Processing with Deep Learning**

主讲：Christopher Manning, John Hewitt

王萱说，斯坦福的自然语言处理课程非常棒，主要介绍深度学习相关知识，从 word2vec 开始，到机器翻译、Transformer、BERT 都有详细的介绍。所有的课程设置也非常合理，包括练习、代码、slides、相关文献官方都做了非常好的整理。

链接：http://web.stanford.edu/class/cs224n/



## 知识图谱

字节跳动知识图谱算法工程师David推荐了两本知识图谱相关的中文书籍和一本英文书，斯坦福CS 520也是业界备受推崇的课程。

**《知识图谱：概念与技术》**

作者：肖仰华

![图片](./imgs/640-1616083658493.png)

这本书系统地介绍知识图谱概念、技术与实践，可以帮助读者建立知识图谱学科体系，贴近工业界的情况。

**《知识图谱》**

作者：赵军

![图片](./imgs/640-1616083658494.png)

知识图谱方面全面综述性的书籍，方方面面都有讲到，是不错的入门书。

**《Linguistic Categorization》**

作者：John R. Taylor

![图片](./imgs/LinguisticCategorization.png)

牛津语言学教科书，涵盖了1987年以来认知语言学的重大发展，也是相对基础的一本书。

**斯坦福CS 520：Knowledge Graphs**

主讲：Vinay K. Chaudhri, Naren Chittar, Michael Genesereth等

饱受好评的斯坦福CS系列，授课人是30余位来自学术界和工业界的专家，在国内也有相当多的簇拥。

链接：https://web.stanford.edu/class/cs520/
## 安卓

在安卓开发Leader JackLin看来，安卓开发者官方网站是最适合初学者的地方，问答社区Stack Overflow中的安卓板块也是一个宝藏平台。

**安卓开发者官方网站**


![图片](https://mmbiz.qpic.cn/mmbiz_png/ia00JJEEnxEloCxtVQjfZ9KuhJaibZNgc2VUoQYN6dY22iag82f6PdpxIVB71eviamkUhuKhwKVDYaNvj9CUJRnZHQ/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

JackLin说，谷歌的安卓官方网站是最为严谨的学习资料，适合绝大多数安卓开发学习者，可以解决安卓初学者的绝大多数问题。

链接：https://developer.android.com/

**Stack Overflow 社区**

![图片](./imgs/640-1616083658541.png)

Stack Overflow是程序员们基本都知道的社区，有很多高质量的问答和资料，社区很活跃，信息全面，也可以看到业界对前沿技术的讨论。

链接：https://stackoverflow.com/questions/tagged/android



## iOS

iOS开发Leader赵子真同样推荐了业内知名的三大技术博客/社区。

**技术博客NSHipster**

![图片](./imgs/640-1616083658542.png)

NSHipster是iOS大神发起的技术博客，涵盖了OC、Swift、Cocoa那些被忽略的特性。

链接：https://nshipster.com/

**技术社区raywenderlich**

赵子真称raywenderlich为「iOS界的百科全书」，它适合初学者，各种tutorial非常浅显易懂。

网站：https://www.raywenderlich.com/ios/articles

视频：https://www.youtube.com/user/rwenderlich/playlists

**iOS社区objc.io**
![图片](./imgs/640-1616083658543.png)

这个社区的内容高质量、深入深入再深入，对国内很多iOS社区影响深远。

链接：https://www.objc.io/

![图片](./imgs/640-1616083658544.png)

## 服务端
服务端Leader安德推荐了4本书，分别从语言、数据库、系统设计几个方面推荐了合适的内容。
**《Concurrency in Go》**

作者：Katherine Cox-Buday

![图片](./imgs/640-1616083658609.png)

Go语言的最强大优势就是并发处理，这本书读了之后能很好地理解Golang的并发能力。

**《Python核心编程（第3版）》**

作者：Wesley Chun

![图片](./imgs/640-1616083658610.png)

推荐后端程序员一定掌握Python这门语言，很多时候能极大地提高工作效率，这里推荐一本工具手册，用于经常查阅。

**《高性能MySQL》**

作者：Baron Schwartz，Peter Zaitsev，Vadim Tkachenko

![图片](./imgs/high_mysql.png)

如果需要了解数据库相关的内容，这本书也是经典著作，推荐大家阅读。

**《代码之髓》**

作者：西尾泰和

![图片](./imgs/640-1616083658614.png)

如果想要提高自己的系统设计能力，不妨读一读这本书，仔细揣摩的话，可以让你的代码写得更加优雅。

## 前端

前端Leader月影推荐了不少在线课程，也有掘金平台的课程。

**HTML: The Living Standard**


![图片](./imgs/640-1616083658616.png)

月影说，这份资料有助于大家理解HTML标准和语义化，打好前端基础。

链接：https://html.spec.whatwg.org/dev/

**MDN Web文档**


这份文档比较全面的介绍最新的 HTML/CSS/JavaScript 标准和特性，非常好的参考资料。

链接：https://developer.mozilla.org/zh-CN/

**玩转CSS的艺术之美**

作者：JowayYoung

![图片](./imgs/640-1616083658648.png)

月影说，这是很不错的一本掘金小册，用简单有趣的方式带领大家玩转CSS。

链接：https://juejin.cn/book/6850413616484040711

**JavaScript高级程序设计（第4版）**

作者：Matt Frisbie

![图片](./imgs/640-1616083658649.png)

几代前端人的JavaScript“红宝书”，李松峰老师翻译。

**你不知道的 Chrome 调试技巧**

作者：Tomek Sułkowski

![图片](./imgs/chrome_skills.png)

一本介绍Chrome调试技巧的免费掘金小册，适合没有使用过Chrome DevTools但是感兴趣的同学，以及想更加深入理解如何用Chrome进行调试的同学。

链接：https://juejin.cn/book/6844733783166418958

**前端工程师进阶 10 日谈**

作者：月影

![图片](./imgs/640-1616083658650.png)

月影自己的创作的掘金小册，内容来自于他多年的前端经验总结，用案例帮你夯实基础、锤炼内功，助你走上前端进阶之路。适合具备基础的HTML、CSS、JavaScript知识，想要变成前端大佬的所有程序员。

链接：https://juejin.cn/book/6891929939616989188

**优秀的前端团队是如何炼成的**

作者：宋小菜前端团队

![图片](./imgs/640-1616083658651.png)

这本小册的作者是宋小菜前端团队的18名成员，以宋小菜前端的成长历史，结合实际案例探讨前端团队从游击队到正规军如何从 0 到 1，帮助即将步入职场的同学少走弯路。

链接：https://juejin.cn/book/6844733800379842574

# 参考

1. [资源帖丨字节跳动技术 Leader 们推荐的学习资源](https://mp.weixin.qq.com/s/mNZHlBcpJDaBkR5J4ay4Tg)
